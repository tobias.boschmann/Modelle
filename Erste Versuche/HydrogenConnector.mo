within ;
connector HydrogenConnector
  import SI = Modelica.SIunits;
  flow SI.MassFlowRate mF;
  SI.Pressure p;

end HydrogenConnector;
